# true if move results in mill

def isMill(to, board, colour):
    #to = 1
	millList = getMillList()
	tmpList = [i for i in millList if to in i]
	for el in tmpList:
		if((el[0] == to or board[el[0]] == colour) and (el[1] == to or board[el[1]] == colour) and (el[2] == to or board[el[2]] == colour)):
			return True
	return False


def getMillList():
	return [
	[0,1,2],
	[8,9,6],
	[16,17,18],
	[7,15,23],
	[19,11,3],
	[22,21,20],
	[14,13,12],
	[6,5,4],
	[2,3,4],
	[10,11,12],
	[18,19,20],
	[1,9,17],
	[21,13,5],
	[16,23,22],
	[8,15,14],
	[0,7,6]
	]