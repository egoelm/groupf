from gameEngine import gameEngine
from platform import *

def numberOfStonesOnBoardForOnePlayer(symbol,board):
    coordinates = []
    rows = 7
    cols = 7

    for row in range(rows):
        for col in range(cols):
            if (board[row][col] == symbol):
                coordinates.append([row, col])

    return len(coordinates)

def create(num, stage):
    if(num == 0):
        eBoard = [[" " , "/" , "/" , " " , "/" , "/" , " "] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,[" " , " " , " " , "/" , " " , " " , " "] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,[" " , "/" , "/" , " " , "/" , "/" , " "]] 
    if(num == 1): 
        eBoard = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , " " , " " , " "] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,[" " , "/" , "/" , " " , "/" , "/" , " "]]    
    else:
        eBoard = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , "O" , " " , " "] ,["/" , "/" , " " , "X" , " " , "/" , "/"] ,["/" , " " , "/" , "O" , "/" , " " , "/"] ,["X" , "/" , "/" , "X" , "/" , "/" , "O"]]    

    board, remove = gameEngine(eBoard,"easy",stage,"X")
    return(remove)     

    