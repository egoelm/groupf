# coding=utf-8
import re
import time
from gameEngine import gameEngine


"""function startGame(playerAI, level):
playerAI = 1, 2 or nothing. 1 sets player1 to AI and 2 sets player2 to AI, nothing is local against local. 
playerAI is equal to playerAI atm
Return 0, 1 or 2. 0 for tie, 1 for player1 winner, 2 for player 2 winner
https://realpython.com/documenting-python-code/#documenting-your-python-code-base-using-docstrings
"""
def startGame(playerAI = 0, level = "easy"): 
    """ Translate argument from Tournament manager to be understandable to 
        Game engine then start the game
        
        If any of the argumenst aren't passed in, the default startGame
        arguments are used.  
        
        Parameters
        ----------
        playerAI : int, optional
                Which player is an AI (default is 0 == no AI player)
        level : string, optional
                Which difficulty level the AI is set to (default to easy)
    """        
    game = GameState(playerAI,level) 
    outcome = game.startGame()   
    return(outcome)#Outcome = 0 if a tie, 1 if player1 won, 2 if player2 won -1 if the tournament should quit         

class Player:
    """ 
        A class used to represent a Player

        Attributes
        ----------
        __stones : int
                Number of stones a player has outside the board
        __symbol : str
                A symbol representing that player on the board

        Methods
        -------   
        getStonesLeft(self)
                    Returns the number of stones a player has left
                    outside the board  
        removeStoneFromBasket(self)
                    Decreases the amount of stones outside of board
                    aka in the basket
        getSymbol(self)
                    Returns the players symbol on the board             
    """
    def __init__(self, symbol):
        """
            Parameters
            ----------
            symbol : str
                    The symbol representing the player on the board
        """
        self.__stones = 4
        self.__symbol  = symbol

    def getStonesLeft(self):
        return self.__stones

    def removeStoneFromBasket(self):
        self.__stones = self.__stones - 1

    def getSymbol(self):
        return self.__symbol


class GameState:
    def __init__(self, playerAI = 0, level = 1 ):
        self.__board = None
        self.__player1 = Player("X")
        self.__player2 = Player("O")
        self.__amount_of_mills = 0
        self.__turn = self.__player1
        self.__countTurns = 0
        self.__playerAI = playerAI
        self.__level = level
        self.__emptyPosition = " "
        self.__invalidPosition = "/"
        self.__exitLose = ""
        self.__quitResign = None

    def startGame(self):

        #self.__level = mapNumberLevelToString(self.__level)

        self.__createBoard()

        while (True):
            #self.__board = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , "O" , " " , " "] ,["/" , "/" , " " , "X" , " " , "/" , "/"] ,["/" , " " , "/" , "O" , "/" , " " , "/"] ,["X" , "/" , "/" , "X" , "/" , "/" , "O"]]    
            #self.__board = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , " " , " " , " "] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,[" " , "/" , "/" , " " , "/" , "/" , " "]]    
            self.__draw_board()
            self.__checkMills()

            if(self.__quitResign != None):

                if(self.__quitResign == 1):

                    print("Player X won on walkover")
                    return 1
                elif(self.__quitResign == 2):
                    print("Player O won on walkover")

                    return 2 
                elif(self.__quitResign == -1):
                    print("You quit the tournament, see you next time!")
                    return -1 

            if (self.__countTurns >= 100):
                print("It's a tie")
                return 0

            if (self.__isLose()):
               
               

                if (self.__turn.getSymbol() == self.__player1.getSymbol()):
                    #print("Winner: O")
                    return 2
                else:
                    #print("Winner: X")
                    return 1

            self.__playTurn()
    def __createBoard(self):
        row = 7
        col = 7
        board = [0] * row
        for i in range(row):
            board[i] = [self.__emptyPosition] * col

        board[0][1], board[0][2], board[0][4], board[0][
            5] = self.__invalidPosition, self.__invalidPosition, self.__invalidPosition, self.__invalidPosition
        board[1][0], board[1][2], board[1][4], board[1][
            6] = self.__invalidPosition, self.__invalidPosition, self.__invalidPosition, self.__invalidPosition
        board[2][0], board[2][1], board[2][5], board[2][
            6] = self.__invalidPosition, self.__invalidPosition, self.__invalidPosition, self.__invalidPosition
        board[3][3] = self.__invalidPosition
        board[4][0], board[4][1], board[4][5], board[4][
            6] = self.__invalidPosition, self.__invalidPosition, self.__invalidPosition, self.__invalidPosition
        board[5][0], board[5][2], board[5][4], board[5][
            6] = self.__invalidPosition, self.__invalidPosition, self.__invalidPosition, self.__invalidPosition
        board[6][1], board[6][2], board[6][5], board[6][
            5] = self.__invalidPosition, self.__invalidPosition, self.__invalidPosition, self.__invalidPosition

        self.__board = board

    def __quitResignFunc(self, coordinates):
        if("r" == coordinates.lower()):
                if(self.__turn.getSymbol() == self.__player2.getSymbol()):
                    self.__quitResign = 1
                elif(self.__turn.getSymbol() == self.__player1.getSymbol()):
                    self.__quitResign = 2
                return True
        if("q" == coordinates.lower()):
                self.__quitResign = -1
                return True
        return False
    def __placeStone(self):

        while (True):
            coordinatesNewStone = input("Where would you like to place your stone? \n")
            if ("help" == coordinatesNewStone.lower()):
                self.__help()
                continue
            if(self.__quitResignFunc(coordinatesNewStone)):
                break
                

            if (not self.__isValidFormat(coordinatesNewStone)):
                self.__invalidInput()
                continue

            coordinatesNewStone = coordinatesNewStone.replace(" ", ",").split(",")

            placeRow = int(coordinatesNewStone[0])
            placeCol = int(coordinatesNewStone[1])

            if (self.__isValidPlace(placeRow, placeCol)):
                self.__board[placeRow][placeCol] = self.__turn.getSymbol()
                self.__turn.removeStoneFromBasket()
                return
            else:
                self.__invalidInput()



    def __removeStone(self):

        removableStones = self.__checkRemovableStones()

        while (True):
            print("Removable stones: ")
            print(removableStones)

            coordinates = input("You got a mill! Which stone would you like to remove? \n")

            if("help" == coordinates.lower()):
                self.__help()
                continue
            if(self.__quitResignFunc(coordinates)):
                break    
            if(not self.__isValidFormat(coordinates)):
                self.__invalidInput()
                continue

            coordinates = coordinates.replace(" ", ",").split(",")

            stoneRow = int(coordinates[0])
            stoneCol = int(coordinates[1])

            if (self.__isValidRemove(stoneRow, stoneCol)):
                self.__board[stoneRow][stoneCol] = self.__emptyPosition
                self.__checkMills()
                return
            else:
                self.__invalidInput()

    def __moveStone(self):

        while (True):

            moveStoneCoordinates = input("Which stone would you like to move and where would you like to move it? \n")
            if("help" == moveStoneCoordinates.lower()):
                self.__help()
                continue
            if(self.__quitResignFunc(moveStoneCoordinates)):
                break
            if(not self.__isValidFormat(moveStoneCoordinates) or len(moveStoneCoordinates) < 7):
                self.__invalidInput()
                continue

            moveStoneCoordinates = moveStoneCoordinates.replace(" ", ",").split(",")

            fromRow = int(moveStoneCoordinates[0])
            fromCol = int(moveStoneCoordinates[1])
            toRow = int(moveStoneCoordinates[2])
            toCol = int(moveStoneCoordinates[3])

            if (self.__isValidmove(fromRow, fromCol, toRow, toCol)):
                self.__board[fromRow][fromCol] = self.__emptyPosition
                self.__board[toRow][toCol] = self.__turn.getSymbol()
                return
            else:
                self.__invalidInput()

    


    def __fly(self):

        while (True):
            moveStoneCoordinates = input("Which stone would you like to move and where would you like to fly? \n")

            if("help" == moveStoneCoordinates.lower()):
                self.__help()
                continue
            if(self.__quitResignFunc(moveStoneCoordinates)):
                break    
            if(not self.__isValidFormat(moveStoneCoordinates) or len(moveStoneCoordinates) < 7):

                self.__invalidInput()
                continue

            moveStoneCoordinates = moveStoneCoordinates.replace(" ", ",").split(",")

            fromRow = int(moveStoneCoordinates[0])
            fromCol = int(moveStoneCoordinates[1])
            toRow = int(moveStoneCoordinates[2])
            toCol = int(moveStoneCoordinates[3])

            if (self.__isValidFly(fromRow, fromCol, toRow, toCol)):
                self.__board[fromRow][fromCol] = self.__emptyPosition
                self.__board[toRow][toCol] = self.__turn.getSymbol()
                return
            else:
                self.__invalidInput()

    def __isValidFly(self, fromRow, fromCol, toRow, toCol):
        return self.__checkIFRealCord(fromRow, fromCol) and self.__checkIFRealCord(toRow,
                                                                                   toCol) and self.__checkIfTurnsCord(
            fromRow, fromCol) and self.__checkIfEmpty(toRow, toCol)

    def __isValidPlace(self, fromRow, fromCol):
        return self.__checkIFRealCord(fromRow, fromCol) and self.__checkIfEmpty(fromRow, fromCol)

    def __isValidRemove(self, fromRow, fromCol):
        return self.__checkIFRealCord(fromRow, fromCol) and self.__checkIfOpponentsCord(fromRow, fromCol) and self.__checkIfFree(fromRow, fromCol)

    def __isValidmove(self, fromRow, fromCol, toRow, toCol):
        return self.__checkIFRealCord(fromRow, fromCol) and self.__checkIFRealCord(toRow,
                                                                                   toCol) and self.__checkIfTurnsCord(
            fromRow, fromCol) and self.__isAdjacentAndEmpty(fromRow, fromCol, toRow, toCol)

    def __invalidInput(self):
        #self.__draw_board()
        print("Invalid coordinates. Write help for more information or try again")

   

    def __getTurnsCoordinates(self):
        coordinates = []
        rows = 7
        cols = 7

        for row in range(rows):
            for col in range(cols):
                if (self.__board[row][col] == self.__turn.getSymbol()):
                    coordinates.append([row, col])

        return coordinates

    def __getOpponentsCoordinates(self):
        coordinates = []
        rows = 7
        cols = 7

        if (self.__turn.getSymbol() == self.__player1.getSymbol()):
            opponentsSymbol = self.__player2.getSymbol()
        else:
            opponentsSymbol = self.__player1.getSymbol()

        for row in range(rows):
            for col in range(cols):
                if (self.__board[row][col] == opponentsSymbol):
                    coordinates.append([row, col])

        return coordinates

    def __isLose(self):


        if(self.__turn.getStonesLeft() > 0):
            return False

        playersTurnCoords = self.__getTurnsCoordinates()


        if (len(playersTurnCoords) > 0 and not self.__canMove(playersTurnCoords)):
            return True

        if (self.__turn.getStonesLeft() + len(playersTurnCoords) < 3):
            return True

        if (len(playersTurnCoords) > 0 and not self.__canMove(playersTurnCoords)):
            return True

        return False

    def __canMove(self, playersTurnCoords):

        for coord in playersTurnCoords:
            if (len(self.__canMoveStone(coord[0], coord[1])) > 0):
                return True

        return False

    def __canMoveStone(self, row, col):
        b = self.__board
        empty = self.__emptyPosition
        if (row == 0):
            if (col == 0 and (b[0][3] == empty or b[3][0] == empty)):
                return [[0, 3], [3, 0]]
            if (col == 3 and (b[0][0] == empty or (b[0][6] == empty) or b[1][3] == empty)):
                return [[0, 0], [0, 6], [1, 3]]
            if (col == 6 and (b[0][3] == empty or b[3][6] == empty)):
                return [[0, 3], [3, 6]]

        if (row == 1):
            if (col == 1 and (b[1][3] == empty or b[3][1] == empty)):
                return [[1, 3], [3, 1]]
            if (col == 3 and (b[1][1] == empty or b[1][5] == empty or b[2][3] == empty or b[0][3] == empty)):
                return [[1, 1], [1, 5], [2, 3], [0, 3]]
            if (col == 5 and (b[1][3] == empty or b[3][5] == empty)):
                return [[1, 3], [3, 5]]

        if (row == 2):
            if (col == 2 and (b[3][2] == empty or b[2][3] == empty)):
                return [[3, 2], [2, 3]]
            if (col == 3 and (b[2][2] == empty or b[2][4] == empty or b[1][3] == empty)):
                return [[2, 2], [2, 4], [1, 3]]
            if (col == 4 and (b[2][3] == empty or b[3][4] == empty)):
                return [[2, 3], [3, 4]]

        if (row == 3):
            if (col == 0 and (b[0][0] == empty or b[6][0] == empty or b[3][1] == empty)):
                return [[0, 0], [6, 0], [3, 1]]
            if (col == 1 and (b[1][1] == empty or b[5][1] == empty or b[3][2] == empty or b[3][0] == empty)):
                return [[1, 1], [5, 1], [3, 2], [3, 0]]
            if (col == 2 and (b[2][2] == empty or b[4][2] == empty or b[3][1] == empty)):
                return [[2, 2], [4, 2], [3, 1]]
            if (col == 4 and (b[2][4] == empty or b[4][4] == empty or b[3][5] == empty)):
                return [[2, 4], [4, 4], [3, 5]]
            if (col == 5 and (b[3][4] == empty or b[3][6] == empty or b[5][5] == empty or b[1][5] == empty)):
                return [[3, 4], [3, 6], [5, 5], [1, 5]]
            if (col == 6 and (b[3][5] == empty or b[0][6] == empty or b[6][6] == empty)):
                return [[3, 5], [0, 6], [6, 6]]

        if (row == 4):
            if (col == 2 and (b[3][2] == empty or b[4][3] == empty)):
                return [[3, 2], [4, 3]]
            if (col == 3 and (b[4][2] == empty or b[5][3] == empty or b[4][4] == empty)):
                return [[4, 2], [5, 3], [4, 4]]
            if (col == 4 and (b[4][3] == empty or b[3][4] == empty)):
                return [[4, 3], [3, 4]]

        if (row == 5):
            if (col == 1 and (b[3][1] == empty or b[5][3] == empty)):
                return [[3, 1], [5, 3]]
            if (col == 3 and (b[5][1] == empty or b[5][5] == empty or b[6][3] == empty or b[4][3] == empty)):
                return [[5, 1], [5, 5], [6, 3], [4, 3]]
            if (col == 5 and (b[5][3] == empty or b[3][5] == empty)):
                return [[5, 3], [3, 5]]

        if (row == 6):
            if (col == 0 and (b[3][0] == empty or b[6][3] == empty)):
                return [[3, 0], [6, 3]]
            if (col == 3 and (b[6][0] == empty or b[6][6] == empty or b[5][3] == empty)):
                return [[6, 0], [6, 6], [5, 3]]
            if (col == 6 and (b[6][3] == empty or b[3][6] == empty)):
                return [[6, 3], [3, 6]]

        return []

    def __countRemaingingStonesOnBoard(self):
        return (len(self.__getTurnsCoordinates()))


    def __checkIFRealCord(self, row, col):
        return (row >= 0 and col >= 0 and row < 7 and col < 7 and self.__board[row][col] != self.__invalidPosition)


    def __checkIfTurnsCord(self, row, col):
        return (self.__board[row][col] == self.__turn.getSymbol())


    def __checkIfOpponentsCord(self, row, col):
        if (self.__turn.getSymbol() == self.__player1.getSymbol()):
            opponentsSymbol = self.__player2.getSymbol()
        else:
            opponentsSymbol = self.__player1.getSymbol()

        return (self.__board[row][col] == opponentsSymbol)


    def __isAdjacentAndEmpty(self, fromRow, fromCol, toRow, toCol):
        adjacentCoords = self.__canMoveStone(fromRow, fromCol)

        for coord in adjacentCoords:

            if (coord[0] == toRow and coord[1] == toCol):
                return self.__checkIfEmpty(toRow, toCol)

        return False


    def __checkIfEmpty(self, row, col):

        if (self.__board[row][col] == self.__emptyPosition):
            return True
        else:
            return False

    def __checkIfFree(self, row, col):

        removableStones = self.__checkRemovableStones()

        for coord in removableStones:
            if row == coord[0] and col == coord[1]:
                return True


    def __checkRemovableStones(self):

        opponentsCoords = self.__getOpponentsCoordinates()
        removableStones = []

        for coord in opponentsCoords:
            if not self.__checkIfStoneIsMill(coord[0], coord[1]):
                removableStones.append(coord)

        if len(removableStones) == 0:
            return False
        else:
            return removableStones


    def __updateBoard(self, newBoard):
        self.__board = newBoard

    def __playTurnAI(self):
        #self.__board = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , " " , " " , " "] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,[" " , "/" , "/" , " " , "/" , "/" , " "]]    
        #self.__draw_board()
        if (self.__turn.getStonesLeft() > 0):
            self.__turn.removeStoneFromBasket()
            board,removedStone=gameEngine(self.__board,self.__level,1,self.__turn.getSymbol())
    
        else:
           
            #self.__draw_board()
            board,removedStone= gameEngine(self.__board,self.__level,2,self.__turn.getSymbol()) 
           
    
        self.__updateBoard(board)
        #self.__draw_board()
      
        if(removedStone):
            row = removedStone[0]
            col = removedStone[1]
            opponentSymbol = self.__player2.getSymbol() if(self.__turn.getSymbol()  == self.__player1.getSymbol()) else self.__player1.getSymbol()

            self.__board[row][col] = opponentSymbol
            
            time.sleep(1.5)
            self.__draw_board()
            print("Player " + self.__turn.getSymbol() +" got a mill!")
            self.__board[row][col] = self.__emptyPosition
            
        time.sleep(1.5)
       

        
        

       
      
      
       
    







        
        

    def __playTurnHuman(self):

        if (self.__turn.getStonesLeft() > 0):
            self.__placeStone()
        else:

            if (self.__countRemaingingStonesOnBoard() == 3):
                self.__fly()
            else:
                self.__moveStone()

        if (self.__checkMills() > 0):
            self.__draw_board()
            self.__removeStone()

    def __playTurn(self):

        if(self.__playerAI == 1 and self.__turn.getSymbol() ==  self.__player1.getSymbol()):
            self.__playTurnAI()
        elif(self.__playerAI == 2 and self.__turn.getSymbol() ==  self.__player2.getSymbol()):
            self.__playTurnAI()
        else:    
            self.__playTurnHuman()

        self.__changeTurn()

    def __changeTurn(self):

        if (self.__turn.getSymbol() == self.__player1.getSymbol()):
            self.__turn = self.__player2

        else:
            self.__turn = self.__player1

        self.__countTurns = self.__countTurns + 1 

    def __checkMills(self):
        newAmountOfMills = self.__count_mills()

        if (newAmountOfMills == self.__amount_of_mills):

            return 0
        elif (newAmountOfMills > self.__amount_of_mills):
            newMills = newAmountOfMills - self.__amount_of_mills

            self.__amount_of_mills = newAmountOfMills
            return newMills
        else:
            self.__amount_of_mills = newAmountOfMills
            return 0

    def __count_mills(self):
        b = self.__board


        millBoard = [
            self.__isEquall(b[0][0],b[0][3],b[0][6]), 
            self.__isEquall(b[1][1],b[1][3],b[1][5]),
            self.__isEquall(b[2][2],b[2][3],b[2][4]),
            self.__isEquall(b[3][0],b[3][1],b[3][2]),
            self.__isEquall(b[3][4],b[3][5],b[3][6]),
            self.__isEquall(b[4][2],b[4][3],b[4][4]),
            self.__isEquall(b[5][1],b[5][3],b[5][5]),
            self.__isEquall(b[6][0],b[6][3],b[6][6]), 
            self.__isEquall(b[0][0],b[3][0],b[6][0]),
            self.__isEquall(b[1][1],b[3][1],b[5][1]),
            self.__isEquall(b[2][2],b[3][2],b[4][2]),
            self.__isEquall(b[0][3],b[1][3],b[2][3]),
            self.__isEquall(b[4][3],b[5][3],b[6][3]),
            self.__isEquall(b[2][4],b[3][4],b[4][4]),
            self.__isEquall(b[1][5],b[3][5],b[5][5]),
            self.__isEquall(b[0][6],b[3][6],b[6][6])
            ]
   
        return sum(millBoard)


    def __checkIfStoneIsMill(self, row, col):
        b = self.__board

        if row == 0 and (col == 0 or col == 3 or col == 6) and self.__isEquall(b[0][0], b[0][3], b[0][6]):
            return True
        if row == 1 and (col == 1 or col == 3 or col == 5) and self.__isEquall(b[1][1], b[1][3], b[1][5]):
            return True
        if row == 2 and (col == 2 or col == 3 or col == 4) and self.__isEquall(b[2][2], b[2][3], b[2][4]):
            return True
        if row == 3 and (col == 0 or col == 1 or col == 2) and self.__isEquall(b[3][0], b[3][1], b[3][2]):
            return True
        if row == 3 and (col == 4 or col == 5 or col == 6) and self.__isEquall(b[3][4], b[3][5], b[3][6]):
            return True
        if row == 4 and (col == 2 or col == 3 or col == 4) and self.__isEquall(b[4][2], b[4][3], b[4][4]):
            return True
        if row == 5 and (col == 1 or col == 3 or col == 5) and self.__isEquall(b[5][1], b[5][3], b[5][5]):
            return True
        if row == 6 and (col == 0 or col == 3 or col == 6) and self.__isEquall(b[6][0], b[6][3], b[6][6]):
            return True
        if col == 0 and (row == 0 or row == 3 or row == 6) and self.__isEquall(b[0][0], b[3][0], b[6][0]):
            return True
        if col == 1 and (row == 1 or row == 3 or row == 5) and self.__isEquall(b[1][1], b[3][1], b[5][1]):
            return True
        if col == 2 and (row == 2 or row == 3 or row == 4) and self.__isEquall(b[2][2], b[3][2], b[4][2]):
            return True
        if col == 3 and (row == 0 or row == 1 or row == 2) and self.__isEquall(b[0][3], b[1][3], b[2][3]):
            return True
        if col == 3 and (row == 4 or row == 5 or row == 6) and self.__isEquall(b[4][3], b[5][3], b[6][3]):
            return True
        if col == 4 and (row == 2 or row == 3 or row == 4) and self.__isEquall(b[2][4], b[3][4], b[4][4]):
            return True
        if col == 5 and (row == 1 or row == 3 or row == 5) and self.__isEquall(b[1][5], b[3][5], b[5][5]):
            return True
        if col == 6 and (row == 0 or row == 3 or row == 6) and self.__isEquall(b[0][6], b[3][6], b[6][6]):
            return True

    def __isEquall(self, a, b, c):
        if (a == self.__emptyPosition):
            return False

        return (a == b and b == c)

    def __draw_player1(self):

        s = "Player X: "
        for stone in range(self.__player1.getStonesLeft()):
            s += "X "

        return s

    def __draw_player2(self):

        s = "Player O: "
        for stone in range(self.__player2.getStonesLeft()):
            s += "O "

        return s

    def __welcome_messege(self):
        print("WELCOME TO THE GAME 9 MEN'S MORRIS\n")
        print("\n")
        print(
            "The board is built up of a coordinate system, to place a stone on the board you write coordinates related to that place, for example if you want to place a stone in the upper right corner you write: 0,6.")
        print("Where the first number is the row and the second is the column.")
        print("All places with symbols belonging to a player is taken, and you can't place stones on taken spots\n")
        print("If you want to stop the game you can write: r")
        print("If you resign a game this way the opponent will win")
        print("To quit the tournament write: q")
        print("If you forgot above information write: help to see it again\n")

    def __help(self):
        print("\n")
        print(
            "The board is built up of a coordinate system, to place a stone on the board you write coordinates related to that place, for example if you want to place a stone in the upper right corner you write: 0,6.")
        print("Where the first number is the row and the second is the column.")
        print("All places with symbols belonging to a player is taken, and you can't place stones on taken spots\n")
        print("If you want to stop the game you can write: r")
        print("If you exit a game this way the opponent will win")

        print("To quit the tournament write: q")


    def __draw_board(self):
    
        print("\n\n")
        print("         0     1      2       3       4      5     6")
        print("    0    "+self.__board[0][0] + " -------------------" + self.__board[0][3] + "------------------- " + self.__board[0][6])
        print("         |                    |                    |")
        print("         |                    |                    |" + "         Stones Left:")
        print("    1    |     " + self.__board[1][1] + "--------------" + self.__board[1][3] + "--------------" +
              self.__board[1][5] + "     |")
        print("         |     |              |              |     |         " + self.__draw_player1())
        print("         |     |              |              |     |")
        print("    2    |     |      " + self.__board[2][2] + "-------" + self.__board[2][3] + "-------" +
              self.__board[2][4] + "      |     |         " + self.__draw_player2())
        print("         |     |      |               |      |     |")
        print("         |     |      |               |      |     |")
        print("    3    " + self.__board[3][0] + "-----" + self.__board[3][1] + "------" + self.__board[3][
            2] + "               " + self.__board[3][4] + "------" + self.__board[3][5] + "-----" + self.__board[3][6])
        print("         |     |      |               |      |     |")
        print("         |     |      |               |      |     |")
        print("    4    |     |      " + self.__board[4][2] + "-------" + self.__board[4][3] + "-------" +
              self.__board[4][4] + "      |     |")
        print("         |     |              |              |     |")
        print("         |     |              |              |     |")
        print("    5    |     " + self.__board[5][1] + "--------------" + self.__board[5][3] + "--------------" +
              self.__board[5][5] + "     |")
        print("         |                    |                    |")
        print("         |                    |                    |")
        print("    6    " + self.__board[6][0] + " -------------------" + self.__board[6][3] + "------------------- " +
              self.__board[6][6])
        print("\n")

        if (self.__turn.getSymbol() == self.__player1.getSymbol()):
            print("TURN: Player X")
        else:
            print("TURN: Player O")

    def __isValidFormat(self, input):
        return re.match("^[0-6],[0-6]($|\s[0-6],[0-6]|\s)", input)


def main():
    startGame(2,'easy')
   
   

if __name__ == "__main__":
    main()
