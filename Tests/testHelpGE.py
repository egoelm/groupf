import sys
sys.path.append("..")
from gameEngine import gameEngine
from platform import *
from boards import *


def includes(coord, coords):
    if(coord):
        return False

    for c in coords:

        if(c[0] == coord[0] and c[1] == coord[1]):
            return(True)

    return False

def getRemovedStone(before,after):
    for i in range(len(before)):
        found = False
        for j in range(len(after)):
          if(after[j][0] == before[i][0] and after[j][1] == before[i][1]):
            found = True
            break

        if(not found):
            return(before[i],i)
        
    return([],None)

def getNewStone(before,after):
    
    for i in range(len(after)):
        found = False
        for y in range(len(before)):
          if(after[i][0] == before[y][0] and after[i][1] == before[y][1]):
            found = True
            break;

        if(not found):
            return(after[i],i)
        
    return([],None)

def isCoordsEquall(before,after):
    if(len(before) != len(after)):
        return False

    for i in range(len(before)):
        if(before[i][0] != after[i][0] or before[i][1] != after[i][1]):
            return False

    return(True)


def coordOnBoardForOnePlayer(symbol,board):
    coordinates = []
    rows = 7
    cols = 7

    for row in range(rows):
        for col in range(cols):
            if (board[row][col] == symbol):
                coordinates.append([row, col])
    
    return(coordinates)


def numberOfStonesOnBoardForOnePlayer(symbol,board):
    return len(coordOnBoardForOnePlayer(symbol,board))

def create(num, stage):
    if(num == 0):
        eBoard = [[" " , "/" , "/" , " " , "/" , "/" , " "] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,[" " , " " , " " , "/" , " " , " " , " "] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,[" " , "/" , "/" , " " , "/" , "/" , " "]] 
    if(num == 1): 
        eBoard = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , " " , " " , " "] ,["/" , "/" , " " , " " , " " , "/" , "/"] ,["/" , " " , "/" , " " , "/" , " " , "/"] ,[" " , "/" , "/" , " " , "/" , "/" , " "]]    
    else:
        eBoard = [["O" , "/" , "/" , " O" , "/" , "/" , "X"] ,["/" , "X" , "/" , "X" , "/" , "O" , "/"] ,["/" , "/" , " " , "" , " " , "/" , "/"] ,[" " , " " , "O" , "/" , "O" , " " , " "] ,["/" , "/" , " " , "X" , " " , "/" , "/"] ,["/" , " " , "/" , "O" , "/" , " " , "/"] ,["X" , "/" , "/" , "X" , "/" , "/" , "O"]]    

    board, remove = gameEngine(eBoard,"easy",stage,"X")
    if(num==0 & len(remove)>0):
        GameState.draw_board(board)

    return(remove)     


if __name__ == "__main__":
    # for levelRound in range(3): 
    #         if(levelRound == 0):
    #             level = "easy"
    #         elif(level == 1):  
    #             level = "medium"
    #         else:
    #             level = "hard"

    #         for i in range(20):
                level = "easy"
                board = randomBoard(3,3)

                HUMANBefore = coordOnBoardForOnePlayer("X",board)
                AIBefore = coordOnBoardForOnePlayer("O",board)
                draw_board(board)

                board, removedStone = gameEngine(board,level,1,"O")
                board[AIBefore[0][0]][AIBefore[0][1]] = " " #Ta bort
                
                draw_board(board)
                if(removedStone):
                    board[removedStone[0]][removedStone[1]] = "X"
                

                HUMANAfter = coordOnBoardForOnePlayer("X",board) 
                AIAfter = coordOnBoardForOnePlayer("O",board)
                
                newStone,newIndex = getNewStone(AIBefore,AIAfter)
                
                if(includes(newStone,HUMANBefore)):
                    print("Level: " + level + ". AI flew to human position")
                    sys.exit()

                if(includes(newStone,AIBefore)):
                    print("Level: " + level + ". No new postion")
                    sys.exit()   

               
                movedStone, movedIndex = getRemovedStone(AIBefore,AIAfter)
                if(len(movedStone) == 0):
                    print("Level: " + level + ". No stone removed/moved")
                    sys.exit()

                 

                del AIBefore[movedIndex]
                del AIAfter[newIndex]

                
                if(not isCoordsEquall(AIBefore,AIAfter)):
                    print("Level: " + level + ". AI before - movedStone  != AI after - new stone")
                    sys.exit()


               

               

               
    
   
    

   

    

        
    
