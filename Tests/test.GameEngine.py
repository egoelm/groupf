import sys
sys.path.append("..")
from testHelpGE  import  * 
from boards import *
import unittest
from gameEngine import gameEngine


#The AI should always play a legal move

#In Setup stage , when stones are placed on the board: Stones can only be placed on vacant points.


class TestsetupStageValidPlace(unittest.TestCase):
    stage = 1

    def test_placeStone(self):
        for levelRound in range(3): 
            if(levelRound == 0):
                level = "easy"
            elif(level == 1):  
                level = "medium"
            else:
                level = "hard"

            for i in range(3):
                board = randomBoard(0,8)

                HUMANBefore = coordOnBoardForOnePlayer("X",board)
                AIBefore = coordOnBoardForOnePlayer("O",board)
                
                board, removedStone = gameEngine(board,level,1,"O")

                if(removedStone):
                    board[removedStone[0]][removedStone[1]] = "X"
                
                HUMANAfter = coordOnBoardForOnePlayer("X",board)
                AIAfter = coordOnBoardForOnePlayer("O",board)

                newStone,index = getNewStone(AIBefore,AIAfter)
                self.assertTrue(newStone, "Level: " + level + " No new one")  
                self.assertTrue(not includes(newStone,HUMANBefore), "Level: " + level + ". New stone placed on human position")
                self.assertTrue(isCoordsEquall(HUMANBefore,HUMANAfter), "Level: " + level + ". Human before  != Human after + possible removed stone" )

                del AIAfter[index]
                self.assertTrue(isCoordsEquall(AIBefore,AIAfter), "Level: " + level + ". AI before  != AI after - new Stones")

                #Vassert if(removedStone not adjacent with newStone);
                
    
    #In Play stage: Stones can only be moved to vacant adjacent points.
    def test_MoveStone(self):
         for levelRound in range(3): 
            if(levelRound == 0):
                level = "easy"
            elif(level == 1):  
                level = "medium"
            else:
                level = "hard"

            for i in range(3):
                board = randomBoard(3,8)

                HUMANBefore = coordOnBoardForOnePlayer("X",board)
                AIBefore = coordOnBoardForOnePlayer("O",board)
        
                board, removedStone = gameEngine(board,level,2,"O")
                #board[AIBefore[0][0]][AIBefore[0][1]] = " " #Ta bort
                
                if(removedStone):
                    board[removedStone[0]][removedStone[1]] = "X"
                
                HUMANAfter = coordOnBoardForOnePlayer("X",board) 
                AIAfter = coordOnBoardForOnePlayer("O",board)
                
                newStone,newIndex = getNewStone(AIBefore,AIAfter)
                
                self.assertTrue(not includes(newStone,HUMANBefore),"Level: " + level + ". AI flew to human position")
                   

                self.assertTrue(not includes(newStone,AIBefore),"Level: " + level + ". No new postion")
                
                movedStone, movedIndex = getRemovedStone(AIBefore,AIAfter)
            
                #self.assertTrue(len(AIBefore) == len(AIAfter) ,"Level: " + level + ". No stone removed/moved")    
                self.assertTrue(movedStone,"Level: " + level + ". No stone removed/moved")
                
                del AIBefore[movedIndex]
                del AIAfter[newIndex]

                
                self.assertTrue(isCoordsEquall(AIBefore,AIAfter),"Level: " + level + ". AI before - movedStone  != AI after - new stone")
        

    #When a player has only three stones left, they can be moved to any vacant point.
    def test_FlyStone(self):
        for levelRound in range(3): 
            if(levelRound == 0):
                level = "easy"
            elif(level == 1):  
                level = "medium"
            else:
                level = "hard"

            for i in range(3):
                board = randomBoard(3,3)

                HUMANBefore = coordOnBoardForOnePlayer("X",board)
                AIBefore = coordOnBoardForOnePlayer("O",board)
        
                board, removedStone = gameEngine(board,level,2,"O")
                #board[AIBefore[0][0]][AIBefore[0][1]] = " " #Ta bort
                
                if(removedStone):
                    board[removedStone[0]][removedStone[1]] = "X"
                
                HUMANAfter = coordOnBoardForOnePlayer("X",board) 
                AIAfter = coordOnBoardForOnePlayer("O",board)
                
                newStone,newIndex = getNewStone(AIBefore,AIAfter)
                
                self.assertTrue(not includes(newStone,HUMANBefore),"Level: " + level + ". AI flew to human position")
                   

                self.assertTrue(not includes(newStone,AIBefore),"Level: " + level + ". No new postion")
                
                movedStone, movedIndex = getRemovedStone(AIBefore,AIAfter)
                self.assertTrue(movedStone,"Level: " + level + ". No stone removed/moved")
                
                del AIBefore[movedIndex]
                del AIAfter[newIndex]

                
                self.assertTrue(isCoordsEquall(AIBefore,AIAfter),"Level: " + level + ". AI before - movedStone  != AI after - new stone")
                  

   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters


if __name__ == '__main__':
    unittest.main()
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
   
#If a mill is generated by the AI, the game engine replies with a board as well as coordinates for the stone that they removed. 
#If the AI plays a move without generating a mill, the game engine should send back a board and the coordinates should be an empty list


#The game engine should give an error message
    #If the game platform sent invalid arguments 
    #If there are missing parameters
