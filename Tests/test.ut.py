import unittest
import sys
sys.path.append("..")
from testHelpGE import create
from gameEngine import gameEngine

class TestGE(unittest.TestCase):
    ##Checks if the GE gives back the right type of arguments when there is no chans to get a mill
    ## See boards in boards.py
    
    def testEmptyBoard(self):
        r = create(0,1)
        if(len(r)>0):
            print(r)
        self.assertTrue(len(r)==0)  
    def testNoPosMill(self):
        r = create(1,1)
        if(len(r)>0):
            print(r)
        self.assertTrue(len(r)==0)
    def testCanGetMill(self):    
        r = create(2,1)
        self.assertTrue(len(r)==0)
        ##In stage 2
    def test2EmptyBoard(self):    
        r = create(0,2)
        self.assertTrue(len(r)==0)
    def test2NoPosMill(self):    
        r = create(1,2)
        self.assertTrue(len(r)==0)
    def test3NoPosMill(self):    
        r = create(2,2)
        self.assertTrue(len(r)==0)
 
if __name__ == '__main__':
    unittest.main()
