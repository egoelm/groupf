import random
from operator import *
import math

from mill import *

# gameEngine runs an ai of difficulty 'difficulty' that takes one legal move on the board and returns the new game state
def gameEngine(board, difficulty, stage, colour):
	#Make sure all arguments are valid.

	colour = parseColour(colour)
	board = parseInBoard(board)

	if (parseArgumets(board, difficulty, stage, colour)):
		
		if (stage == 1):
			#Placing stones (pre-game)
			removed = placeStone(board, difficulty, colour)
		else:
			#making moves
			removed = makeMove(board, difficulty, colour)

		return(parseOutBoard(board), parseRemove(removed))
	else:
		print('Arguments not valid!')


# parseArgumets checks so that all inputs are correct.
def parseArgumets(board, diff, stage, colour):
	numFails = 0

	if not board:
		print('You forgot the board argument, stupid')
		print('How do you expect me to make a move if I cant see the board!!?')
		numFails += 1

	if not diff:
		print('You forgot the difficulty argument, stupid')
		numFails += 1

	if not stage:
		print('You forgot the stage argument, stupid')
		print('Are placing stones or moving?')
		numFails += 1

	if not 'colour' in locals():
		print('You forgot the colour argument, stupid')
		print('I need to know which colour I am!!!')
		numFails += 1

	if numFails:
		if numFails > 2:
			print('Have you even read the documentation??')
			print('You function call is missing %i arguments!!' %numFails)
		return False

	#CHECK BOARD PARAMETER VALIDITY
	if not (len(board) == 24):
		print('The board isnt valid')
		return False

	#CHECK DIFF PARAMETER VALIDITY
	if not (diff == 'easy' or diff == 'medium' or diff == 'hard' ):
		print('The difficulty isnt valid: ', diff)
		return False

	#CHECK STAGE PARAMETER VALIDITY
	if not (stage == 1 or stage == 2):
		print('The stage isnt valid')
		return False

	if not (colour == 1 or colour == 0):
		print('The different colours arent valid')
		print('The players must have either 1 or 0 as player marker')

	return True

# placeStone returns the new board after color tried to take a move during stage 1. The kind of move depends on diff.
def placeStone(board, diff, colour):
	values = [5, 4, 2, 1]
	removed = -1

	#Advanced logic depending on diff
	if (diff == 'easy'):
		moves = findAvailablePlacements(board)

		#Random move
		move = random.choice(moves)
		if(isMill(move[1], board, colour)):
			movesR = removableStones(board, colour, values)
			if(movesR != []):
				board[movesR[-1][0]] = -1
				removed = movesR[-1][0]
			#board[move[0]] = -1
		board[move[1]] = colour
		return(removed)


	if(diff == 'medium'):
		moves = findAvailablePlacements(board)
		moves = valueMoves(board, moves, colour, values, 1)

		move = moves[(random.randint(-(math.ceil(len(moves)/2)), -1))]
		if(isMill(move[1], board, colour)):
			movesR = removableStones(board, colour, values)
			if(movesR != []):
				board[movesR[-1][0]] = -1
				removed = movesR[-1][0]
			#board[move[0]] = -1
		board[move[1]] = colour
		return(removed)

	else:
		move = minmaxPlacement(board, colour, values)

		if(isMill(move[1], board, colour)):
			movesR = removableStones(board, colour, values)
			if(movesR != []):
				board[movesR[-1][0]] = -1
				removed = movesR[-1][0]
			#board[move[0]] = -1
		board[move[1]] = colour
		return(removed)

# findAvailablePlacements returns a matrix with all free spaces of the board
def findAvailablePlacements(board):
	indexList = []
	for index, item in enumerate(board):
		if (item == -1):
			indexList.append([-1,index,0])
	return indexList

# makeMove returns the new board after color tried to take a move. The kind of move depends on diff.
def makeMove(board, diff, colour):
	movesP = getMoves(board, colour)
	move = None
	values = [10,5,1,2]
	removed = -1

	if(movesP != []):
		moves = valueMoves(board, movesP, colour, values, 2)
		if(diff == 'easy'):
			move = moves[(random.randint(0, (math.floor(len(moves)/2))))]
			board[move[0]] = -1
			board[move[1]] = colour


	if(diff == 'medium'):
		if(movesP != []):
			moves = valueMoves(board, movesP, colour, values, 2)
			move = moves[(random.randint(-(math.ceil(len(moves)/2)), 0))]
			board[move[0]] = -1
			board[move[1]] = colour
	#hard
	elif(diff == 'hard'):
		values = [15,8,1,0]
		if(movesP != []):
			# minmax(board, colour, movesP, values, 2)
			# moves = valueMoves(board, movesP, colour, values, 2)
			move = minmax(board, colour, values)

			board[move[0]] = -1
			board[move[1]] = colour
	if move:
		if(isMill(move[1], board, colour)):
			movesR = removableStones(board, colour, values)
			if(movesR != []):
				board[movesR[-1][0]] = -1
				removed = movesR[-1][0]

	return(removed)

# removableStones returns a matrix of stones color can remove if a mill was created. Format: [[index(from), index(to), value of stone]]
def removableStones(board, colour, values):
	opponent = 0
	if(colour == 0):
		opponent = 1
	moves = getMoves(board, opponent)
	moves = valueMoves(board, moves, colour, values, 2)
	if(moves == []):
		for index, item in enumerate(board):
			if(item == opponent):
				moves.append([index,0,0])
	deleteMoves = []
	for index, item in enumerate(moves):
			if(isMill(item[0], board, opponent)):
				deleteMoves.append(item)
	for item in deleteMoves:
		moves.remove(item)
	return(moves)


# getStoneCount returns the amount of stones clolour has in play
def getStoneCount(board, colour):
	count = 0
	for _, item in enumerate(board):
		if(item == colour):
			count += 1
	return(count)

# getStones returns a list of indexes of all stones colour has in play
def getStones(board, colour):
	indexList = []
	for index, item in enumerate(board):
		if(item == colour):
			indexList.append(index)
	return(indexList)

# getMoves returns a array with all possible moves colour can take. Format: [[index(from), index(to), value of move]]
def getMoves(board, colour):
	moves = []
	if(getStoneCount(board, colour) > 3):
		adjacentMatrix = getAdjacentMatrix()
		for index, item in enumerate(board):
			if(item == colour):
				for i in adjacentMatrix[index]:
					if(board[i] == -1):
						moves.append([index,i,0])
						#moves = [index = vilken sten vi rör, i = till vilket index vi går, 0 = poäng ]
	else:
		stones = getStones(board, colour)
		positions = findAvailablePlacements(board)
		for index, item in enumerate(stones):
			for i, jtem in enumerate(positions):
				moves.append([item,jtem[1],0])

	return(moves)

#values = list of values for different end positions(create mill, hinder enemy mill, road connections, friendly neighbor)
def valueMoves(board, moves, colour, values, stage):
	adjacentMatrix = getAdjacentMatrix()

	#If [colour == 0] equals to true, opponent becomes 1.
	opponent = (0, 1)[colour == 0]

	if(stage == 1):
		for index, item in enumerate(moves):
			if(isMill(item[1], board, colour)):
				moves[index][2] += values[0]
			if(isMill(item[1], board, opponent)):
				moves[index][2] += values[1]

			moves[index][2] += (values[2] * len(adjacentMatrix[item[1]]))
			moves[index][2] -= (values[2] * len(adjacentMatrix[item[0]]))

			for i, place in enumerate(adjacentMatrix[item[1]]):
				if(board[place] == colour):
					moves[index][2] += values[3]
			for i, place in enumerate(adjacentMatrix[item[0]]):
				if(board[place] == colour):
					moves[index][2] -= values[3]

	elif(stage == 2):
		for index, item in enumerate(moves):
			boardTMP = list(board)
			boardTMP[item[0]] = -1

			if(isMill(item[1], boardTMP, colour)):
				moves[index][2] += values[0]
			if(isMill(item[1], boardTMP, opponent)):
				moves[index][2] += values[1]
			if(isMill(item[0], board, opponent)):
				moves[index][2] -= values[1]

			moves[index][2] += (values[2] * len(adjacentMatrix[item[1]]))
			moves[index][2] -= (values[2] * len(adjacentMatrix[item[0]]))

			for i, place in enumerate(adjacentMatrix[item[1]]):
				if(board[place] == colour):
					moves[index][2] += values[3]
			for i, place in enumerate(adjacentMatrix[item[0]]):
				if(board[place] == colour):
					moves[index][2] -= values[3]

	return (sorted(moves, key=itemgetter(2)))

def minmax(board, colour, values):

	depth = 2
	evaluatedPaths = []

	#Get possible moves
	movesP = getMoves(board, colour)

	#Evalue each move
	moves = valueMoves(board, movesP, colour, values, 1)
	moves.reverse()

	for index, move in enumerate(moves):
		boardCopy = board.copy()
		pathScore = checkMove(index, move, boardCopy, colour, values, depth, 0, iterator = 0)
		pathScore += move[2]
		evaluatedPaths.append(pathScore)

	ix = evaluatedPaths.index(max(evaluatedPaths))
	move = moves[ix]
	return move

def checkMove(index, move, board, colour, values, depth, score, iterator):
	opponent = (1, 0)[colour == 1]

	if iterator == depth:
		return score

	#"My" move
	board[move[0]] = -1
	board[move[1]] = colour

	if(isMill(move[1], board, colour)):
		movesR = removableStones(board, colour, values)
		if(movesR != []):
			board[movesR[-1][0]] = -1

	if getStoneCount(board, opponent) < 3:
		return score + (200 / (iterator + 1))

	#Opponent move
	movesP = getMoves(board, opponent)

	if len(movesP) == 0:
		return score + (200 / (iterator + 1))

	else:
		moves = valueMoves(board, movesP, opponent, values, 2)
		move = moves[-1]
		board[move[0]] = -1
		board[move[1]] = opponent

		if(isMill(move[1], board, opponent)):
			movesR = removableStones(board, opponent, values)
			if(movesR != []):
				board[movesR[-1][0]] = -1

		# My possible moves
		movesP = getMoves(board, colour)
		if len(movesP) == 0:
			return score - (200 / (iterator + 1))
		else:
			moves = valueMoves(board, movesP, colour, values, 2)

		tempScore = 0
		for index, move in enumerate(moves):

			newScore = checkMove(index, move, board, colour, values, depth, move[2], iterator + 1)

			if tempScore < newScore:
				tempScore = newScore

		return tempScore

def minmaxPlacement(board, colour, values):

	depth = 2
	evaluatedPaths = []

	#Get possible moves
	movesP = findAvailablePlacements(board)
	if movesP:
		#Evalue each move
		moves = valueMoves(board, movesP, colour, values, 1)
		moves.reverse()

		for index, move in enumerate(moves):
			boardCopy = board.copy()
			pathScore = checkPlacement(index, move, boardCopy, colour, values, depth, 0, iterator = 0)
			pathScore += move[2]
			evaluatedPaths.append(pathScore)

		ix = evaluatedPaths.index(max(evaluatedPaths))
		move = moves[ix]
		return move
	print('Something went terribly wrong in findAvailablePlacements!')
	return


def checkPlacement(index, move, board, colour, values, depth, score, iterator):
	opponent = (1, 0)[colour == 1]

	if iterator == depth:
		return score

	#"My" move
	board[move[0]] = -1
	board[move[1]] = colour

	if(isMill(move[1], board, colour)):
		movesR = removableStones(board, colour, values)
		if(movesR != []):
			board[movesR[-1][0]] = -1

	# if getStoneCount(board, opponent) < 3:
	# 	return score + (200 / (iterator + 1))

	#Opponent move

	movesP = findAvailablePlacements(board)

	if len(movesP) == 0:
		return score + (200 / (iterator + 1))

	else:
		moves = valueMoves(board, movesP, opponent, values, 1)
		move = moves[-1]
		board[move[0]] = -1
		board[move[1]] = opponent

		if(isMill(move[1], board, opponent)):
			movesR = removableStones(board, opponent, values)
			if(movesR != []):
				board[movesR[-1][0]] = -1

		# My possible moves
		movesP = findAvailablePlacements(board)
		if len(movesP) == 0:
			return score - (200 / (iterator + 1))
		else:
			moves = valueMoves(board, movesP, colour, values, 1)

		tempScore = 0
		for index, move in enumerate(moves):

			newScore = checkPlacement(index, move, board, colour, values, depth, move[2], iterator + 1)

			if tempScore < newScore:
				tempScore = newScore

		return tempScore


# The adjacent matrix gives the index of a nodes neighbours. matrix[0] gives node 0s neighbours
def getAdjacentMatrix():
	return [
		[1, 7],
		[0, 2, 9],
		[1, 3],
		[2, 11, 4],
		[3, 5],
		[4, 13, 6],
		[5, 7],
		[6, 15, 0],
		[15, 9],
		[8, 1, 10, 17],
		[9, 11],
		[10, 3, 12, 19],
		[11, 13],
		[21, 12, 5, 14],
		[13, 15],
		[7, 8, 23, 14],
		[17, 23],
		[9, 18, 16],
		[17, 19],
		[18, 11, 20],
		[19, 21],
		[20, 13, 22],
		[21, 23],
		[22, 15, 16]
		]


def parseOutBoard(board):
	dim = 7
	parsingMatrix = getParsingMatrix()
	ans = [["/" for x in range(dim)] for y in range(dim)]
	for i in range(0,24,1):
		index = parsingMatrix[i]
		if(board[i] == 0):
			ans[index[0]][index[1]] = 'X'
		elif(board[i] == 1):
			ans[index[0]][index[1]] = 'O'
		else:
			ans[index[0]][index[1]] = " "	
	return ans

def parseInBoard(inBoard):
	board = []
	parsingMatrix = getParsingMatrix()
	for i in range(0,24,1):
		index = parsingMatrix[i]
		stone = inBoard[index[0]][index[1]]
		if(stone == 'X'):
			board.append(0)
		elif(stone == 'O'):
			board.append(1)
		else:
			board.append(-1)
	return board

def parseColour(colour):
	if(colour == 'X'):
		return 0
	else:
		return 1

def parseDiff(diff):
	if(diff == 0):
		return "easy"
	elif(diff == 1):
		return "medium"
	else:
		return "hard"

def parseRemove(removeID):
	removed = []
	parsingMatrix = getParsingMatrix()
	if(removeID != -1):
		removed = parsingMatrix[removeID]
	return(removed)

def getParsingMatrix():
	return [
		[0,0],
		[0,3],
		[0,6],
		[3,6],
		[6,6],
		[6,3],
		[6,0],
		[3,0],
		[1,1],
		[1,3],
		[1,5],
		[3,5],
		[5,5],
		[5,3],
		[5,1],
		[3,1],
		[2,2],
		[2,3],
		[2,4],
		[3,4],
		[4,4],
		[4,3],
		[4,2],
		[3,2]
		]


"""if __name__== '__main__':
    #board=[[" ","/","/"," ","/","/"," "],["/"," ","/"," ","/"," ","/"],["/","/"," "," "," ","/","/"],[" "," "," ","/"," "," "," "],["/","/"," "," "," ","/","/"],["/"," ","/"," ","/"," ","/"],[" ","/" ,"/"," ","/","/"," "]]#board = [-1, 1, 1, 0, 0, -1, 0, 1, 1, 1, 1, -1, 0, 0, 1, -1, -1, 0, 0, 0, -1, -1, 1, -1]
    whiteWins = 0
    blackWins = 0
    tie = 0
    white = 'easy'
    black = 'hard'

for _ in range(0, 1, 1):
	#board=[[" ","/","/"," ","/","/"," "],["/"," ","/"," ","/"," ","/"],["/","/"," "," "," ","/","/"],[" "," "," ","/"," "," "," "],["/","/"," "," "," ","/","/"],["/"," ","/"," ","/"," ","/"],[" ","/" ,"/"," ","/","/"," "]]
	#board = parseInBoard(board)
	#[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
	board=[[' ', '/', '/', ' ', '/', '/', ' '], ['/', ' ', '/', ' ', '/', ' ', '/'], ['/', '/', ' ', ' ', ' ', '/', '/'], [' ', ' ', ' ', '/', ' ',' ', ' '], ['/', '/', ' ', ' ', ' ', '/', '/'], ['/', ' ', '/', ' ', '/', ' ', '/'], [' ', '/', '/', ' ', ' ', '/', ' ']]

for _ in range(0,8,1):
	board, remove = gameEngine(board, white, 1, "X")
	board, remove = gameEngine(board, black, 1, "O")
	iter = 0	

while(len(list(x for x in board if x == 0)) > 2 and len(list(x for x in board if x == 1)) > 2 and iter < 69):
	tmpBoard = board[:]
	board = gameEngine(board, white, 2, 1)[:]
	if(tmpBoard == board):
		blackWins += 1
		tie -= 1
		break
	tmpBoard = board[:]
	board = gameEngine(board, black, 2, 0)
	
	if(tmpBoard == board):
		whiteWins += 1
		tie -= 1
		break
	iter += 1
	
if(not(len(list(x for x in board if x == 0)) > 2)):
	whiteWins += 1
elif(not(len(list(x for x in board if x == 1)) > 2)):
	blackWins += 1
else:
	tie += 1

print("whiteWins: ")
print(whiteWins)
print("blackWins: ")
print(blackWins)
print("tie:" )
print(tie)
"""